require('trent.set')
require('trent.remap')
require('trent.packer')

-- set theme to mellow
vim.cmd('colorscheme mellow')
