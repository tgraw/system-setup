vim.g.mapleader = " "

-- Moves selected lines in visual mode down or up
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Join next line to this one, then jump back to invoking position 
vim.keymap.set("n", "J", "mzJ'z")

-- When searching, moves to next or previous match, centers the screen on the match
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- duplicate visual selection without interfering with clipboard
vim.keymap.set("x", "<leader>p", [["_dP]])

-- Find and replace whats under cursor
vim.keymap.set("n", "<C-f>", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "<leader>vpp", "<cmd>e ~/.dotfiles/nvim/.config/nvim/lua/trent/packer.lua<CR>");


-- Swap between dark and light mode respectively
vim.keymap.set("n", "<leader>bgd", function() 
                                     vim.opt.background = 'dark' 
                                     vim.cmd('colorscheme mellow') 
                                   end)
vim.keymap.set("n", "<leader>bgl", function() 
                                        vim.opt.background = 'light' 
                                        vim.cmd('colorscheme catppuccin') 
                                    end)

-- Close current buffer
vim.keymap.set("n", "<leader>c", "<cmd>:bd<CR>")
