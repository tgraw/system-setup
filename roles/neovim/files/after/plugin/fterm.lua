local ft = require("FTerm")

ft.setup({
    border = 'double',
    dimensions = {
        height = 0.8,
        width = 0.9
    },
})

vim.keymap.set('n', '<C-\\>', function()
    ft.toggle()
end)
vim.keymap.set('t', '<C-\\>', function()
    ft.toggle()
end)
