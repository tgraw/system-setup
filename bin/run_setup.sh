REPOSITORY_URI="gitlab.com/tgraw/system-setup"

CONFIG_DIR="$HOME/.config/dotfiles"
VAULT_SECRET="$HOME/.ansible-vault/vault.secret"
DOTFILES_DIR="$HOME/.dotfiles"
SSH_DIR="$HOME/.ssh"
IS_FIRST_RUN="$HOME/.dotfiles_run"

set -e

if ! dpkg -s sudo >/dev/null 2>&1; then
    echo -e "=> Installing missing dependencies"    
    echo -e "   > Updating APT repos..."
    apt-get update 2>&1 > /dev/null

    echo -e "   > Installing sudo, python3-launchpadlib, git"
    apt-get install -y sudo python3-launchpadlib git 2>&1 > /dev/null
fi

# Install ansible if not installed
if ! dpkg -s ansible >/dev/null 2>&1; then
    echo -e "=> Installing ansible..."
    echo -e "   > Updating APT repos..."
    sudo apt-get update 2>&1 > /dev/null
    echo -e "   > Installing dependency: software-properties-commons..."
    sudo apt-get install -y software-properties-common 2>&1 > /dev/null
    echo -e "   > Adding ansible PPA repo..."
    sudo apt-add-repository -y ppa:ansible/ansible 2>&1 > /dev/null
    echo -e "   > Updating APT repos..."
    sudo apt-get update 2>&1 > /dev/null
    echo -e "   > Installing ansible..."
    sudo apt-get install -y ansible 2>&1 > /dev/null
fi

# Generate SSH keys
if ! [[ -f "$SSH_DIR/authorised_keys" ]]; then
    echo -e "=> Generating SSH keys"
    mkdir -p "$SSH_DIR"

    chmod 700 "$SSH_DIR"
    ssh-keygen -b 4096 -t rsa -f "$SSH_DIR/id_rsa" -N "" -C "$USER@$HOSTNAME" 2>&1 > /dev/null

    cat "$SSH_DIR/id_rsa.pub" >> "$SSH_DIR/authorised_keys"
fi

# Clone this repository
if ! [[ -d "$DOTFILES_DIR" ]]; then
    echo -e "=> Cloning Repository: $REPOSITORY_URI"
    git clone --quiet "https://$REPOSITORY_URI.git" "$DOTFILES_DIR" 2>&1 > /dev/null
else

    echo -e "=> Updating Repository: $REPOSITORY_URI"
    git -C "$DOTFILES_DIR" pull --quiet > /dev/null
fi

# Push dotfiles onto stack so can come back later
pushd "$DOTFILES_DIR" 2>&1 > /dev/null

# Update ansible-galaxy
echo -e "=> Updating galaxy..."
ansible-galaxy install -r requirements.yml 2>&1 > /dev/null

# Run playbook
echo -e "Running playbook..."
if [[ -f $VAULT_SECRET ]]; then
    echo -e "Using vault config file..."
    ansible-playbook --vault-password-file $VAULT_SECRET "$DOTFILES_DIR/main.yml" "$@"
else
    echo -e "!! Vault config file not found..."
    ansible-playbook "$DOTFILES_DIR/main.yml" "$@"
fi

popd 2>&1 > /dev/null

if ! [[ -f "$IS_FIRST_RUN" ]]; then
    echo -e "First run complete!"
    echo -e "=> Please reboot your computer to complete the setup."
    touch "$IS_FIRST_RUN"
fi
